import os

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "rah",
        "USER": "rah",
        "PASSWORD": "adminpassword123",
        "HOST": "localhost",
        "PORT": "8085",
    }
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {"console": {"class": "logging.StreamHandler"}},
    "loggers": {
        "django": {"level": "INFO"},
        "qinspect": {"level": "DEBUG"},
        "faker": {"level": "INFO"},
        "boto3": {
            "level": "DEBUG"
        },  # to see if you run boto3 operations. Should not typically happen anyway.
        "": {"handlers": ["console"], "level": os.getenv("DJANGO_LOG_LEVEL", "INFO")},
    },
}

ALLOWED_HOSTS = "*"
