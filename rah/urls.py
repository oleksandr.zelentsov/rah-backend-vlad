"""rah URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework.documentation import include_docs_urls
from rest_framework.permissions import AllowAny

from server_info.views import changelog, service_info

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "docs/",
        include_docs_urls(title="Rahivnycia API", permission_classes=[AllowAny]),
    ),
    path("api/accounts/", include("accounts.urls")),
    path("api/debts/", include("debts.urls")),
    path("api/payments/", include("payments.urls")),
    path("", changelog, name='changelog'),
    path("server-info/", service_info, name='service_info'),
]
