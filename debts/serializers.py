from rest_framework import serializers

from accounts.serializers import BaseUserSerializer
from debts.models import Debt


class DebtSerializer(serializers.ModelSerializer):
    user_1 = BaseUserSerializer()
    user_2 = BaseUserSerializer()

    class Meta:
        model = Debt
        fields = (
            "id",
            "user_1",
            "user_2",
            "debt_value",
            "debt_value_currency",
        )
