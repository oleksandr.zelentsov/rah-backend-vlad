from functools import partial
from itertools import combinations
from random import choices, randrange
from typing import List

from debts.models import Debt
from utils.helpers import money_is_close


def validate_debt_info(debt_info, users):
    for user_1_idx, user_2_idx, debt_value in debt_info:
        debt = Debt.objects.get(user_1=users[user_1_idx], user_2=users[user_2_idx])
        assert money_is_close(debt.debt_value, debt_value)


def debt_info_to_model_data(debt_info, users) -> List[Debt]:
    debts = []
    for user_1_idx, user_2_idx, debt_value in debt_info:
        debts.append(
            Debt(
                user_1=users[user_1_idx],
                user_2=users[user_2_idx],
                debt_value=debt_value,
            )
        )

    return Debt.objects.bulk_create(debts)


def generate_debts(users, user_pair_generator=partial(combinations, r=2)):
    debts = []
    len_users = len(users)
    debt_value_weights = [len_users, len_users / 2]
    for user_1, user_2 in user_pair_generator(users):
        debt_value, = choices([0, randrange(10, 100)], weights=debt_value_weights)
        if debt_value:
            debt = Debt(user_1=user_1, user_2=user_2, debt_value=debt_value)
            debts.append(debt)

    return debts
