from django.urls import path

from debts.views import DebtListView, DebtDetailView

urlpatterns = [
    path("", DebtListView.as_view(), name="debt_list"),
    path("<int:user_id>/", DebtDetailView.as_view(), name="debt_detail"),
]
