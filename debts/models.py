# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal
from typing import Union

from django.conf import settings
from django.db import models
from djmoney.models.fields import MoneyField

from accounts.models import User


class DebtManager(models.Manager):
    def involved_debts(self, users, both=True):
        qs = self.get_queryset()
        if both:
            return qs.filter(models.Q(user_1__in=users) & models.Q(user_2__in=users))
        else:
            return qs.filter(models.Q(user_1__in=users) | models.Q(user_2__in=users))

    def related_debts(self, to_user: Union[User, int]):  # TODO test
        return self.get_queryset().filter(models.Q(user_1=to_user) | models.Q(user_2=to_user)).annotate(
            owed_to_the_related=models.Case(
                models.When(
                    user_2=to_user,
                    then=models.Value("true")
                ),
                default=models.Value("false"),
                output_field=models.BooleanField()
            )
        ).order_by("owed_to_the_related")

    def get_related_debt(self, user: User, to_user_id: Union[User, int]):
        return self.related_debts(to_user_id).get(
            models.Q(user_1=user, user_2=to_user_id)
            | models.Q(user_2=user, user_1=to_user_id)
        )


class RoundedDebtManager(DebtManager):
    def get_queryset(self):
        return super().get_queryset().filter(debt_value__gte=Decimal("0.01"))


class Debt(models.Model):
    objects = DebtManager()
    rounded_objects = RoundedDebtManager()
    user_1 = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="owes_debts"
    )
    user_2 = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="owed_debts"
    )
    debt_value = MoneyField(max_digits=19, decimal_places=4, currency_choices=settings.CURRENCY_CHOICES, default_currency=settings.CURRENCIES[0])
    added_automatically = models.BooleanField(
        default=False,
        verbose_name="was the debt added within the graph minimization algorithm",
    )

    def __str__(self):
        return f"{self.debt_value}: {self.user_1} -> {self.user_2}"

    class Meta:
        unique_together = ("user_1", "user_2")
