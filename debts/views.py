from django.http import Http404
from rest_framework.generics import ListAPIView, RetrieveAPIView

from debts.models import Debt
from debts.serializers import DebtSerializer


class DebtListView(ListAPIView):
    serializer_class = DebtSerializer

    def get_queryset(self):
        return Debt.rounded_objects.related_debts(self.request.user)


class DebtDetailView(RetrieveAPIView):
    serializer_class = DebtSerializer

    def get_object(self):
        other_user_id = self.kwargs["user_id"]
        try:
            return Debt.objects.get_related_debt(self.request.user, other_user_id)
        except Debt.DoesNotExist:
            raise Http404
