from model_mommy.recipe import Recipe

from debts.models import Debt
from utils.helpers import random_money


debt_recipe = Recipe(Debt, debt_value=random_money)
