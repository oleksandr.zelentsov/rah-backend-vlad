# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from debts.models import Debt

admin.site.register(Debt)
