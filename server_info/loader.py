import os

from django.conf import settings


def load_text_file_contents(filename: str) -> str:
    with open(filename, 'r') as fp:
        return ''.join(fp.readlines())


DEFAULT_DIR = os.path.join(settings.BASE_DIR, "server_info", "templates", "markdown")


MARKDOWN_TEMPLATE_NAMES = (
    "server-info.md",
)

MARKDOWN_TEMPLATES = {
    name: load_text_file_contents(os.path.join(DEFAULT_DIR, name))
    for name in MARKDOWN_TEMPLATE_NAMES
}
