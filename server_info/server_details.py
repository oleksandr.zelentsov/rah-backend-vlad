from _pydecimal import Decimal

from attr import dataclass
from django.apps import apps
from django.db.models import Sum, Avg


@dataclass
class ServerDetails:
    user_count: int
    payment_count: int
    average_payment_value: Decimal
    average_relative_payment_value: Decimal
    average_debtor_value: Decimal
    average_debtor_real_value: Decimal

    def as_verbose_dict(self) -> dict:
        return {
            "User count": self.user_count,
            "Payment count": self.payment_count,
            "Average payment value": self.average_payment_value,
            "Average payment relative value": self.average_relative_payment_value,
            "Average debtor relative value": self.average_debtor_value,
            "Average debtor real value": self.average_debtor_real_value,
        }

    @classmethod
    def build(cls):
        User = apps.get_model("accounts", "User")
        Payment = apps.get_model("payments", "Payment")
        payment_info = Payment.objects.annotate(
            relative_payment_value=Sum('debtor__relative_value'),
        ).aggregate(
            avg_payment_value=Avg('payment_value', ),
            avg_debtor_value=Avg('debtor__relative_value', ),
            avg_relative_payment_value=Avg('relative_payment_value')
        )
        user_count = User.objects.count()
        payment_count = Payment.objects.count()
        average_payment_value = round(payment_info.get("avg_payment_value"), 2)
        average_relative_payment_value = round(payment_info.get("avg_relative_payment_value"), 2)
        average_debtor_relative_value = round(payment_info.get("avg_debtor_value"), 2)
        average_debtor_real_value = round(
            average_payment_value
            / average_relative_payment_value
            * average_debtor_relative_value, 2
        )
        return cls(
            user_count=user_count,
            payment_count=payment_count,
            average_payment_value=average_payment_value,
            average_relative_payment_value=average_relative_payment_value,
            average_debtor_value=average_debtor_relative_value,
            average_debtor_real_value=average_debtor_real_value,
        )
