import os

from django.conf import settings
from django.shortcuts import render
from django.template.loader import render_to_string
from rest_framework.reverse import reverse

from server_info.loader import DEFAULT_DIR
from server_info.server_details import ServerDetails


def render_markdown_page(request, markdown_template_name: str, context=None):
    context = context or {}

    rendered_template = render_to_string(
        os.path.join(DEFAULT_DIR, markdown_template_name),
        context=context,
        request=request,
    )
    new_context = dict(context, content=rendered_template)
    return render(request, "markdown-page.html", new_context)


def changelog(request):
    return render(
        request,
        'markdown-page.html',
        {
            'content': settings.CHANGELOG_CONTENTS,
            "navigation_urls": {
                "Server information": reverse("service_info"),
            }
        }
    )


def service_info(request):
    si = ServerDetails.build()
    return render_markdown_page(
        request,
        'server-info.md', {
            "title": "Server information",
            "navigation_urls": {
                "Changelog": reverse("changelog"),
            },
            "server_info": si.as_verbose_dict(),
        },
    )
