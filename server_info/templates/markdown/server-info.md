# Server info

<table>
    {% for param_name, param_value in server_info.items %}
    <tr>
        <td>
            <b>{{ param_name }}</b>
        </td>
        <td>
            {{ param_value }}
        </td>
    </tr>
    {% endfor %}
</table>
