import itertools

from model_mommy.recipe import Recipe, seq

from accounts.models import User


def emails(prefix="test", suffix="example.com"):
    for i in itertools.count():
        yield f"{prefix}+{i}@{suffix}"


user_recipe = Recipe(User, username=seq("username"), email=emails())

fictional_user_recipe = user_recipe.extend(email=None, first_name=seq("first name "), fictional=True)

staff_user_recipe = user_recipe.extend(is_staff=True)

super_user_recipe = staff_user_recipe.extend(is_superuser=True)
