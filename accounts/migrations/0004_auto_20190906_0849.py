# Generated by Django 2.2.1 on 2019-09-06 08:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20190518_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='fictional',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=254, null=True, unique=True),
        ),
        migrations.AddConstraint(
            model_name='user',
            constraint=models.CheckConstraint(check=models.Q(models.Q(('email__isnull', True), ('fictional', True)), ('email__isnull', False), _connector='OR'), name='user_email_not_null'),
        ),
    ]
