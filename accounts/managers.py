import logging

from django.contrib.auth.models import UserManager as DjangoUserManager
from django.db.models import F, Q, QuerySet, Value, CharField
from django.db.models.functions import Concat

from utils.django.managers import ActiveManager

logger = logging.getLogger(__name__)


class UserManager(DjangoUserManager):
    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)

    def find_by_string(self, line: str) -> QuerySet:
        line_stripped = line.strip()
        return (
            self.get_queryset()
            .annotate(full_name=Concat(F("first_name"), Value(" ", output_field=CharField()), F("last_name")))
            .filter(
                Q(full_name=line_stripped)
                | Q(username=line_stripped)
                | Q(email=line_stripped)
                | Q(last_name=line_stripped)
                | Q(first_name=line_stripped)
            )
        )

    def find_by_string_or_create_fictional(
        self, line: str, author_user=None, return_tuple=True
    ):
        """
        Returns a tuple of the user instance and a boolean, which indicates that the user is new.
        """
        user = self.find_by_string(line).first()
        to_create = not user
        if to_create:
            logger.warning("could not find the user with name: %s", line)
            logger.warning("creating a fictional user")
            first_name, *last_name = line.split()
            last_name = ' '.join(last_name)
            user = self.create(
                email=None,
                fictional=True,
                first_name=first_name,
                last_name=last_name,
                imported_by=author_user,
            )

        if return_tuple:
            return user, to_create
        else:
            return user


class InactiveUserManager(UserManager, ActiveManager):
    pass


class ActiveUserManager(UserManager, ActiveManager):
    def __init__(self, *args, **kwargs):
        ActiveManager.__init__(self, active=True)
        DjangoUserManager.__init__(self, *args, **kwargs)
