# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from typing import List

from django.contrib.auth.models import AbstractUser
from django.db import models

from accounts.managers import InactiveUserManager, ActiveUserManager, UserManager
from utils.django.managers import ArchivedManager
from utils.django.models import ArchivedModel


class User(ArchivedModel, AbstractUser):
    objects = UserManager()
    inactive_users = InactiveUserManager(active=False)
    active_users = ActiveUserManager()
    archived_users = ArchivedManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS: List[str] = []

    username = models.CharField(max_length=150, unique=False)
    email = models.EmailField(null=True, unique=True)

    fictional = models.BooleanField(default=False)
    imported_by = models.ForeignKey(
        "accounts.User", on_delete=models.DO_NOTHING, blank=True, null=True
    )

    def __repr__(self):
        ident = self.ident()
        return f"<User: {ident}>"

    def ident(self):
        return self.get_full_name() or self.username or self.email

    def __str__(self):
        return self.ident()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=(
                    models.Q(fictional=True, email__isnull=True) |
                    models.Q(fictional=False, email__isnull=False)
                ),
                name="user_email_not_null",
            ),
            models.CheckConstraint(
                check=(
                    models.Q(fictional=True, first_name__isnull=False)
                    & ~models.Q(first_name__exact="")
                )
                | models.Q(fictional=False),
                name="first_name_not_null_when_fictional",
            ),
            models.CheckConstraint(
                check=models.Q(fictional=True, imported_by__isnull=False)
                | models.Q(fictional=False),
                name="imported_by_not_null_when_fictional",
            ),
        ]
