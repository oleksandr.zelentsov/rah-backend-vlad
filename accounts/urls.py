from django.conf.urls import url

from accounts.views import RegisterView, AcquireTokenView, TokenRefreshView

urlpatterns = [
    url("login/", AcquireTokenView.as_view()),
    url("register/", RegisterView.as_view()),
    url('refresh/', TokenRefreshView.as_view())
]
