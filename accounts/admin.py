# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from accounts.models import User


class UserAdmin(DjangoUserAdmin):
    fieldsets = None
    list_display = ("id", "email", "first_name", "last_name")
    fields = ("email", "username", "first_name", "last_name", "password", "is_staff", "is_active", "is_superuser", "fictional", "imported_by")
    ordering = ("id",)


admin.site.register(User, UserAdmin)
