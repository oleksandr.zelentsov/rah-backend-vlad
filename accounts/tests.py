import pytest

from accounts.models import User


@pytest.mark.django_db
def test_create_superuser_basic():
    user = User._default_manager.create_superuser(
        "lolka@lol.com", "c0mpl1cAt3D", is_staff=True, is_superuser=True
    )
    assert User.objects.get(pk=user.id) == user


@pytest.mark.django_db
def test_nullable_unique_email(api_user):
    u1, u2 = (
        User(
            email=None, fictional=True, first_name="Viktor Tsoy", imported_by=api_user
        ),
        User(
            email=None, fictional=True, first_name="Harry Potter", imported_by=api_user
        ),
    )

    User.objects.bulk_create([u1, u2])
