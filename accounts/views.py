import rest_framework
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from accounts.models import User
from accounts.serializers import AcquireTokenSerializer, UserRegisterSerializer, TokenRefreshSerializer


class RegisterView(CreateAPIView):
    """Use this to create a new user."""

    serializer_class = UserRegisterSerializer
    queryset = User.inactive_users
    permission_classes = (rest_framework.permissions.AllowAny,)


class TokenRefreshView(CreateAPIView):
    """Use this to refresh your token."""
    permission_classes = (rest_framework.permissions.AllowAny,)
    authentication_classes = ()
    serializer_class = TokenRefreshSerializer


class AcquireTokenView(CreateAPIView):
    """Use this to log in."""

    serializer_class = AcquireTokenSerializer
    queryset = User.active_users
    permission_classes = (rest_framework.permissions.AllowAny,)
