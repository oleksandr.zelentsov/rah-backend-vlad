from django.contrib.auth import authenticate
from django.db.models import Q
from django.utils.translation import ugettext
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework.fields import EmailField
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.serializers import PasswordField
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken
from six import text_type

from accounts.models import User
from utils.django.serializers import NonUniqueFieldsAnymoreSerializer

USER_FIELDS = ("id", "email", "first_name", "last_name")


class BaseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = USER_FIELDS


class UserRegisterSerializer(NonUniqueFieldsAnymoreSerializer, BaseUserSerializer):
    non_unique_fields = ["email"]

    class Meta:
        model = User
        fields = USER_FIELDS + ("password",)
        extra_kwargs = {"password": {"write_only": True}}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["email"].validators += [UniqueValidator(User.inactive_users)]
        self.user = None

    def validate(self, attrs):
        attrs = super().validate(attrs)

        try:
            self.user = User.objects.get(Q(email=attrs["email"]))
        except User.DoesNotExist:
            pass  # We're okay, no such data
        else:
            if self.user.is_active:
                self.user = None
                raise ValidationError(detail=ugettext('User with such or similar credentials already exists.'))

        return attrs

    def create(self, validated_data):
        if self.user is not None:
            self.update_user(self.user, validated_data)
        else:
            self.user = User.objects.create_user(is_active=True, **validated_data)

        # TODO send email
        return self.user

    @staticmethod
    def update_user(user: User, validated_data: dict):
        for k, v in validated_data.items():
            setattr(user, k, v)

        user.save()


class AcquireTokenSerializer(UserRegisterSerializer):
    email = EmailField(write_only=True)
    password = PasswordField(write_only=True)

    access_token = serializers.CharField(read_only=True)
    refresh_token = serializers.CharField(read_only=True)
    user = BaseUserSerializer(read_only=True)
    # settings = ServiceSettingsSerializer(read_only=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user_ = None
        self.fields["email"].validators = []

    def validate(self, attrs):
        return BaseUserSerializer.validate(self, attrs)

    def create(self, validated_data):
        user = authenticate(self.context["request"], **validated_data)
        if user is None:
            raise AuthenticationFailed()

        user_ = user.__dict__
        user_.update(TokenService(user).generate_token())
        return user_

    class Meta:
        model = User
        fields = (
            "email",
            "password",
            "access_token",
            "refresh_token",
            "user",
        )


class TokenRefreshSerializer(serializers.Serializer):
    access_token = serializers.CharField(read_only=True)
    refresh_token = serializers.CharField(min_length=1)

    @staticmethod
    def validate_refresh_token(value):
        try:
            return RefreshToken(value)
        except TokenError:
            raise ValidationError(ugettext("Token is invalid"), code=401)

    def create(self, validated_data):
        return TokenService.refresh_token(validated_data['refresh_token'])

    def update(self, instance, validated_data):
        raise NotImplementedError

    class Meta:
        fields = (
            "access_token",
            "refresh_token",
        )


class TokenService:
    def __init__(self, user):
        self.user = user

    def generate_token(self) -> dict:
        refresh = RefreshToken.for_user(self.user)
        refresh["user"] = BaseUserSerializer(instance=self.user).data
        data = {
            "refresh_token": text_type(refresh),
            "access_token": text_type(refresh.access_token),
        }
        return data

    @staticmethod
    def refresh_token(refresh_token):
        if not isinstance(refresh_token, RefreshToken):
            refresh_token = RefreshToken(refresh_token)

        data = {
            'access_token': str(refresh_token.access_token),
            'refresh_token': None,
        }

        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh_token.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh_token.set_jti()
            refresh_token.set_exp()

            data['refresh_token'] = str(refresh_token)

        return data
