from typing import List

from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator


class NonUniqueFieldsAnymoreSerializer(ModelSerializer):
    non_unique_fields: List[str] = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in self.non_unique_fields:
            self.fields[field_name].validators = [
                validator
                for validator in self.fields[field_name].validators
                if not isinstance(validator, UniqueValidator)
            ]
