from abc import abstractmethod
from typing import Union

from django.db import models


class FilteredManager(models.Manager):
    """
    Returns a filtered queryset.
    Redefine get_filter() to specify the filter conditions.
    Use ``get_inverse_queryset()`` to exclude items, met with the defined filter.
    """

    @abstractmethod
    def get_filter(self) -> Union[models.Q, dict]:
        pass

    def get_queryset(self):
        basic_queryset = super().get_queryset()
        filter_ = self.get_filter()
        func = basic_queryset.filter
        return self._distinguish_filter_type(filter_, func)

    def get_inverse_queryset(self):
        basic_queryset = super().get_queryset()
        filter_ = self.get_filter()
        func = basic_queryset.exclude
        return self._distinguish_filter_type(filter_, func)

    @staticmethod
    def _distinguish_filter_type(filter_, func):
        if isinstance(filter_, dict):
            return func(**filter_)
        elif isinstance(filter_, models.Q):
            return func(filter_)
        else:
            raise ValueError(
                f"get_filter() has to return either a dict or a Q object,"
                f" instead returns {filter_.__class__.__name__}"
            )


class ArchivedManager(FilteredManager):
    def __init__(self, archived=True, archived_field_name="archived", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._are_items_archived = archived
        self._archived_field_name = archived_field_name

    def get_filter(self):
        return {self._archived_field_name: self._are_items_archived}


class ActiveManager(FilteredManager):
    def __init__(
        self, active: bool = True, active_field_name="is_active", *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self._are_items_active = active
        self._active_field_name = active_field_name

    def get_filter(self):
        return {self._active_field_name: self._are_items_active}
