from django.db import models

from utils.django.managers import ArchivedManager


class ArchivedModel(models.Model):
    objects = models.Manager()
    all_unarchived = ArchivedManager(False)
    all_archived = ArchivedManager()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    archived = models.BooleanField(default=False)

    def _set_archived(self, archived=True, call_save=True):
        self.archived = archived
        if call_save:
            return self.save()

    def set_archived(self, call_save=True):
        return self._set_archived(archived=True, call_save=call_save)

    def set_restored(self, call_save=True):
        return self._set_archived(archived=False, call_save=call_save)

    class Meta:
        abstract = True
