import functools
import math
import random
import secrets
from datetime import date, time, timedelta
from decimal import Decimal
from itertools import filterfalse, tee
from string import digits
from typing import Union, Any

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.utils import timezone
from djmoney.money import Money

currency_round = functools.partial(round, ndigits=4)
money_is_close = functools.partial(math.isclose, abs_tol=0.0001)


def text_currency_to_float(text):
    t = text
    dot_pos = t.rfind(".")
    comma_pos = t.rfind(",")
    if comma_pos > dot_pos:
        t = t.replace(".", "")
        t = t.replace(",", ".")
    else:
        t = t.replace(",", "")

    return Decimal(t)


def get_money(x: Any, currency=None) -> Union[Decimal, Money]:
    """
    Rounds and formats number of money.
    If currency is provided, the number x is also converted to Money instance.

    Otherwise, the Decimal instance is returned.
    """
    if isinstance(x, Money):
        return currency_round(x)

    if isinstance(x, str):
        x = text_currency_to_float(x)

    result = currency_round(x)
    if currency is not None:
        result = Money(result, currency)

    return result


def random_money():
    return Money(random.randrange(5, 2000), random.choice(settings.CURRENCIES))


def random_from(coll):
    return functools.partial(random.choice, coll)


def years_ago(years):
    return date.today() - relativedelta(years=years)


def get_next_datetime_with_time(t: time, now=None):
    if now is None:
        now = timezone.now()
    elif callable(now):
        now = now()

    time_kwargs = dict(
        hour=t.hour, minute=t.minute, second=t.second, microsecond=t.microsecond
    )
    if now.time() < t:
        return now.replace(time_kwargs)
    elif now.time() > t:
        return (now + timedelta(days=1)).replace(time_kwargs)

    return now


def random_choice(n: int = 1, alphabet=digits):
    return "".join([secrets.choice(alphabet) for _ in range(n)])


def partition(pred, iterable, wrapper=None):
    """Use a predicate to partition entries into false entries and true entries"""
    # partition(is_odd, range(10)) --> 1 3 5 7 9    and    0 2 4 6 8
    t1, t2 = tee(iterable)
    res1 = filterfalse(pred, t1)
    res2 = filter(pred, t2)
    if wrapper is not None:
        res1, res2 = wrapper(res1), wrapper(res2)

    return res1, res2


def trim_str_left(s1, s2):
    if s1.startswith(s2):
        return s1[len(s2) :]

    return s1


def trim_str_right(s1, s2):
    if s1.endswith(s2):
        return s1[: len(s1) - len(s2)]

    return s1
