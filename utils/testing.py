from attr import dataclass
from model_mommy import mommy

from accounts.models import User


class Mommy(mommy.Mommy):
    """
    Hacks Mommy & Django into thinking we can specify "auto_now_add" on create.
    """

    def make(self, _save_kwargs=None, _refresh_after_create=False, **attrs):
        obj = super().make(_save_kwargs, _refresh_after_create, **attrs)
        self.__handle_created(attrs, obj)
        return obj

    def prepare(self, _save_related=False, **attrs):
        obj = super().prepare(_save_related, **attrs)
        self.__handle_created(attrs, obj, __save=False)
        return obj

    def __handle_created(self, attrs, obj, __save=True):
        if "created" in attrs:
            val = attrs["created"]
            if callable(val):
                val = val()

            obj.created = val
            if __save:
                obj.save()


@dataclass
class FakeRequest:
    user: User

    @classmethod
    def build_context(cls, *args, **kwargs) -> dict:
        return {"request": cls(*args, **kwargs)}
