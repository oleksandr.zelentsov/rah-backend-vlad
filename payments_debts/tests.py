from dataclasses import dataclass, field
from typing import List, Set

import pytest

from accounts.models import User
from accounts.recipes import user_recipe, fictional_user_recipe
from debts.models import Debt
from graphs.services import update_user_debts
from payments.models import Payment, Debtor
from payments.testing_utils import generate_payments_and_debtors
from payments_debts.services import dissolve_fictional_user


@dataclass
class UserData:
    user: User = field(hash=False, compare=False)
    payments: List[Payment]
    debtors: List[Debtor]
    debts: Set[Debt]

    @classmethod
    def from_user(cls, user: User):
        return cls(
            user=user,
            payments=list(Payment.objects.filter(payer=user)),
            debtors=list(Debtor.objects.filter(user=user)),
            debts=set(Debt.objects.related_debts(user)),
        )


@pytest.mark.django_db
def test_dissolve_fictional_user():
    admin = user_recipe.make()
    real_users = user_recipe.make(_quantity=5)
    users = fictional_user_recipe.make(imported_by=admin, _quantity=5)
    p, pd = generate_payments_and_debtors(users)
    update_user_debts()

    user_datas = [
        UserData.from_user(user)
        for user in users
    ]

    for user, user_data in zip(users, user_datas):
        assert user_data.user == user
        assert all([payment.payer == user for payment in user_data.payments])
        assert all([debtor.user == user for debtor in user_data.debtors])
        assert all([user in (debt.user_1, debt.user_2) for debt in user_data.debts])

    for user, real_user in zip(users, real_users):
        dissolve_fictional_user(user, real_user)

    real_user_datas = [
        UserData.from_user(user)
        for user in real_users
    ]
    for real_user_data, user_data in zip(real_user_datas, user_datas):
        assert real_user_data == user_data
