from django.apps import AppConfig


class PaymentsDebtsConfig(AppConfig):
    name = "payments_debts"
