import functools
import itertools
import logging
import operator
import random
from collections import defaultdict
from numbers import Number
from typing import Collection, NamedTuple, Dict, Union, Tuple, Optional

from django.db import transaction
from django.db.models import Q, QuerySet
from djmoney.money import Money

from accounts.models import User
from debts.models import Debt
from payments.errors import MultipleCurrenciesError
from payments.models import Payment, Debtor
from utils.helpers import get_money, money_is_close

logger = logging.getLogger(__name__)


class CalculationResult(NamedTuple):  # TODO remove when tests are written
    sum_total: Union[Number, int]
    total: Union[Number, int]
    people: Dict[User, Money]
    currency: Optional[str]

    @classmethod
    def none(cls):
        return CalculationResult(0, 0, {}, None)

    @classmethod
    def from_payment_list(cls, payments: "Collection[Payment]") -> "CalculationResult":
        if not payments:
            return cls.none()

        calculated_payments = map(Payment.calculate, payments)
        people_to_their_money: Dict[User, Money] = defaultdict(lambda: zero)
        currencies = set()  # TODO use multiple currencies
        for person_to_rvalue in calculated_payments:
            if not person_to_rvalue:
                continue
            payment_currency = next(iter(person_to_rvalue.values())).currency
            logger.debug("adding %s", payment_currency)
            currencies.add(payment_currency)
            zero = Money(0, payment_currency)
            for user, money in person_to_rvalue.items():
                debt_val = get_money(money, payment_currency)
                logger.debug("debt value = %s", debt_val)
                people_to_their_money[user] += debt_val

        total = sum([payment.payment_value for payment in payments])
        return CalculationResult(
            sum_total=sum([money for person, money in people_to_their_money.items()]),
            total=total,
            people=people_to_their_money,
            currency=total.currency,
        )


class PaymentService:
    @classmethod
    def get_users_involved_in_payment(cls, payment: Payment):  # TODO test
        return {payment.payer} | {
            debtor.user for debtor in Debtor.objects.filter(payment=payment)
        }

    @classmethod
    def get_users_involved_in_payments(cls, payments: Collection[Payment]):  # TODO test
        return functools.reduce(
            operator.or_, map(cls.get_users_involved_in_payment, payments)
        )

    @classmethod
    def get_debt(
        cls,
        user1: User,
        user2: User,
        all_people_calculation_results: Dict[User, CalculationResult] = None,
        currency=None,
    ) -> Union[None, Debt]:
        if all_people_calculation_results is None:
            users = [user1, user2]
            all_people_calculation_results, currency = cls.get_aggregated_calculations(
                users, cls.get_payments_of_users(users)
            )

        assert currency is not None

        zero = Money(0, currency)
        p1_owes_p2 = all_people_calculation_results[user2].people.get(user1, zero)
        p2_owes_p1 = all_people_calculation_results[user1].people.get(user2, zero)

        assert isinstance(
            p1_owes_p2, Money
        ), f"error: {p1_owes_p2}; type: {p1_owes_p2.__class__.__name__}"
        assert isinstance(
            p2_owes_p1, Money
        ), f"error: {p2_owes_p1}; type: {p2_owes_p1.__class__.__name__}"

        if money_is_close(p1_owes_p2, p2_owes_p1):
            return None
        elif p1_owes_p2 < p2_owes_p1:
            who_owes = user2
            who_is_owed = user1
            debt_value = get_money(p2_owes_p1 - p1_owes_p2, currency)
        else:  # elif p1_owes_p2 > p2_owes_p1:
            who_owes = user1
            who_is_owed = user2
            debt_value = get_money(p1_owes_p2 - p2_owes_p1, currency)

        return Debt(debt_value=debt_value, user_1=who_owes, user_2=who_is_owed)

    @classmethod
    def get_payments_of_users(
        cls, users: Collection[User], payments: QuerySet = None
    ):  # TODO test
        if payments is None:
            payments = Payment.objects.all()

        return payments.filter(Q(payer__in=users) | Q(debtors__in=users))

    @classmethod
    def get_aggregated_calculations(
        cls, involved_users, payments
    ) -> Tuple[Dict[User, CalculationResult], str]:
        people_calculations = {}
        currencies = set()
        for user in involved_users:
            users_payments = [payment for payment in payments if payment.payer == user]
            if not users_payments:
                people_calculations[user] = CalculationResult.none()
                continue

            calculation_result = CalculationResult.from_payment_list(
                users_payments
            )
            currencies.add(calculation_result.currency)
            people_calculations[user] = calculation_result

        currencies = {x for x in currencies if x}
        assert currencies
        return people_calculations, random.choice(list(currencies))

    @classmethod
    def generate_debts_from_payment_history(
        cls, payments: "Collection[Payment]", involved_users: "Collection[User]" = None
    ) -> Collection[Debt]:
        """
        Generates debts from payment list.
        Does not actually create the records in the db.
        Use Debt.bulk_create to create them.
        """
        if len(payments) == 0:
            return []

        all_currencies = {payment.payment_value_currency for payment in payments}
        if len(all_currencies) != 1:
            raise MultipleCurrenciesError(all_currencies)

        if involved_users is None:
            involved_users = cls.get_users_involved_in_payments(payments)

        all_people_calculations, currency = cls.get_aggregated_calculations(
            involved_users, payments
        )
        debts = []
        for person1, person2 in itertools.combinations(involved_users, 2):
            debt = cls.get_debt(person1, person2, all_people_calculations, currency)
            if debt is not None:
                debts.append(debt)

        return debts


def dissolve_fictional_user(
    fictional_user: User,
    assignee: User,
    delete_fictional_user: bool = True,
    auto_confirm_payer_debtors: bool = True
) -> None:
    """
    Re-assigns payments, debtors and debts from
    a fictional user to an existing real user.
    """
    if not fictional_user.fictional:
        raise ValueError("first argument should be a fictional user")

    if assignee.fictional:
        raise ValueError("second argument should be a not fictional user")

    with transaction.atomic():
        reassigned_payments: int = Payment.objects.filter(
            payer=fictional_user,
        ).update(
            payer=assignee,
        )
        logger.info("reassigned %s payments", reassigned_payments)

        reassigned_debtors = Debtor.objects.filter(
            user_id=fictional_user.id,
        ).update(
            user=assignee,
        )
        logger.info("reassigned %s debtors", reassigned_debtors)
        if auto_confirm_payer_debtors:
            auto_confirmed_debtors = Debtor.objects.get_payer_debtors().filter(
                user=assignee,
            ).update(confirmed=True)
            logger.info("auto confirmed %s debtors", auto_confirmed_debtors)

        reassigned_debts = Debt.objects.filter(
            user_1=fictional_user,
        ).update(
            user_1=assignee,
        )
        reassigned_debts += Debt.objects.filter(
            user_2=fictional_user,
        ).update(
            user_2=assignee,
        )
        logger.info("reassigned %s debts", reassigned_debts)

        if delete_fictional_user:
            fictional_user.delete()
