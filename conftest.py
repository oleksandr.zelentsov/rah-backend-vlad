import pytest

from accounts.models import User


@pytest.fixture()
@pytest.mark.django_db
def api_user() -> User:
    return User.objects.create(email="test@test.com")
