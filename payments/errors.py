from typing import Collection


class MultipleCurrenciesError(Exception):
    def __init__(self, currencies: Collection[str]):
        super().__init__(f"Cannot use multiple currencies in this context: {','.join(currencies)}")
