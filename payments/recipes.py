from functools import partial
from random import randrange, choice

from model_mommy.recipe import Recipe, related, foreign_key

from accounts.recipes import user_recipe
from payments.models import Payment, Debtor
from utils.helpers import random_money, random_from

random_relative_value = random_from([1, 2, 3, 4])

payment_recipe = Recipe(Payment, payment_value=random_money)

payment_with_debtors_recipe = payment_recipe.extend(
    debtors=related(payment_recipe, user_recipe)
)

debtor_recipe = Recipe(
    Debtor,
    payment=foreign_key(payment_recipe),
    user=foreign_key(user_recipe),
    relative_value=random_relative_value,
    confirmed=False,
)

confirmed_debtor_recipe = debtor_recipe.extend(confirmed=True)
