from collections import defaultdict
from random import choice, sample, randrange
from typing import List, Dict

from accounts.recipes import user_recipe
from payments.models import Payment, Debtor


def generate_payments_and_debtors(
    users, number=10, value_range: range = range(50, 2000), payment_impact=0.4
):
    payers = [choice(users) for _ in range(number)]
    payments: List[Payment] = []
    payments_debtors: Dict[Payment, List[Debtor]] = defaultdict(list)
    for payer in payers:
        payment = Payment.objects.create(
            payer=payer, payment_value=choice(value_range), added_to_graph=False
        )
        debtors_users = sample(set(users) - {payer}, round(len(users) * payment_impact))
        for debtor_user in debtors_users:
            debtor = payment.add_debtor(
                user=debtor_user, relative_value=randrange(10, 100), confirmed=True
            )
            payments_debtors[payment].append(debtor)

        payments.append(payment)

    return payments, payments_debtors


def payments_info_to_model_data(payment_info, single_payment=False):
    users = user_recipe.make(_quantity=len(payment_info[0]) - 2)
    payments = []
    for payment_value, payer_idx, *debtor_relative_values in payment_info:
        payment = Payment.objects.create(
            payer=users[payer_idx], payment_value=payment_value
        )
        for user_idx, relative_value in enumerate(debtor_relative_values):
            if relative_value:
                debtor_user = users[user_idx]
                payment.add_debtor(
                    user=debtor_user, relative_value=relative_value, confirmed=True
                )

        payments.append(payment)

    if single_payment:
        return payments[0], users

    return payments, users
