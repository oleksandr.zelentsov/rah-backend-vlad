from decimal import Decimal
from typing import Tuple

import pytest
from djmoney.money import Money

from accounts.recipes import user_recipe
from debts.models import Debt
from debts.testing_utils import validate_debt_info
from graphs.services import update_user_debts
from payments.models import Payment, Debtor
from payments.recipes import debtor_recipe, payment_recipe
from payments.testing_utils import payments_info_to_model_data
from payments_debts.services import PaymentService
from utils.helpers import money_is_close

# total, r_values, debts
DEBTOR_TEST_DATA = [
    (35, (3, 2, 3), (13.1250, 8.75, 13.1250)),
    (50, (2, 2.5, 0.5), (20, 25, 5)),
]
DEBTS_FROM_PAYMENTS_DATA = (
    (
        ((50, 0, 0, 10, 20, 20),),  # TOTAL, payer_idx, *debtors
        (  # Debts: who_owes_idx, who_is_owed_idx, debt_value
            (1, 0, 10),
            (2, 0, 20),
            (3, 0, 20),
        ),
    ),
    (
        ((50, 0, 0, 1, 0, 0), (30, 1, 1, 0, 0, 0)),  # TOTAL, payer_idx, *debtors
        ((1, 0, 20),),  # Debts: who_owes_idx, who_is_owed_idx, debt_value
    ),
)
REAL_VALUE_COEFFICIENT_DATA = [
    (  # (TOTAL, payer_idx, *debtors), real_value_coefficient
        (50, 0, 10, 20, 20, 0, 0),
        1,
    ),
    ((100, 0, 1, 2, 1, 0, 0), 25),
    ((50, 0, 1, 2.5, 1, 2, 0.5), 7.1428),
]
DEBT_RELATIVE_VALUE_SUM_DATA = [
    (  # (TOTAL, payer_idx, *debtors), debt_relative_value
        (50, 0, 10, 20, 20, 0, 0),
        50,
    ),
    ((100, 0, 1, 2, 1, 0, 0), 4),
    ((50, 0, 1, 2.5, 1, 2, 0.5), 7),
]


@pytest.mark.django_db
@pytest.mark.parametrize("total,r_values,debts", DEBTOR_TEST_DATA)
def test_calculate_payment(total: float, r_values: Tuple[float], debts: Tuple[float]):
    debtor_count = len(r_values)
    payer, *users = user_recipe.make(_quantity=debtor_count + 1)

    payment: Payment = payment_recipe.make(payer=payer, payment_value=total)
    debtor_recipe.make(
        payment=payment,
        user=iter(users),
        relative_value=iter(r_values),
        confirmed=True,
        _quantity=debtor_count,
    )

    calculation_dict = payment.calculate()
    for user, debt in zip(users, debts):
        assert money_is_close(calculation_dict[user], debt)


@pytest.mark.django_db
@pytest.mark.parametrize("payment_info,debt_info", DEBTS_FROM_PAYMENTS_DATA)
def test_payment_service_debts_basic(payment_info, debt_info):
    payments, users = payments_info_to_model_data(payment_info)

    debts = PaymentService.generate_debts_from_payment_history(payments, users)
    Debt.objects.bulk_create(debts)

    validate_debt_info(debt_info, users)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payment_info,real_value_coefficient", REAL_VALUE_COEFFICIENT_DATA
)
def test_payment_real_value_coefficient(payment_info, real_value_coefficient):
    payment, _ = payments_info_to_model_data([payment_info], single_payment=True)
    assert money_is_close(payment.real_value_coefficient, real_value_coefficient)


@pytest.mark.django_db
def test_payment_relative_value_to_real_value():
    payment, _ = payments_info_to_model_data([(20, 0, 1, 1, 2)], single_payment=True)
    assert payment.real_value_to_relative_value(Money(5)) == Decimal("1")
    assert payment.relative_value_to_real_value(1) == Money(5, payment.payment_value_currency)


@pytest.mark.django_db
def test_payment_debtor():
    payment, users = payments_info_to_model_data(
        [(20, 0, 1, 1, 2)], single_payment=True
    )
    assert payment.payer_debtor.user == users[0]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payment_info,debt_relative_value_sum", DEBT_RELATIVE_VALUE_SUM_DATA
)
def test_debt_relative_value_sum(payment_info, debt_relative_value_sum):
    payment, _ = payments_info_to_model_data([payment_info], single_payment=True)
    assert payment.debt_relative_value_sum == debt_relative_value_sum


@pytest.mark.django_db
def test_debt_sources():
    u1, u2 = user_recipe.make(_quantity=2)
    payments = [
        Payment.objects.create(payer=u1, payment_value=Money(10, "PLN"), description="test1"),
        Payment.objects.create(payer=u2, payment_value=Money(10, "PLN"), description="test2"),
    ]

    d01 = payments[0].add_debtor(u1, 1, confirmed=True)
    d02 = payments[0].add_debtor(u2, 1, confirmed=True)

    d12 = payments[1].add_debtor(u2, 1, confirmed=True)
    d11 = payments[1].add_debtor(u1, 1, confirmed=True)

    update_user_debts()

    p1, n1 = Debtor.objects.get_debt_sources(u1)
    p2, n2 = Debtor.objects.get_debt_sources(u2)

    assert (d01 not in p1) and (d01 not in n1) and (d01 not in p2) and (d01 not in n2)
    assert (d02 in p1) and (d02 not in n1) and (d02 not in p2) and (d02 in n2)
    assert (d12 not in p1) and (d12 not in n1) and (d12 not in p2) and (d12 not in n2)
    assert (d11 not in p1) and (d11 in n1) and (d11 in p2) and (d11 not in n2)


@pytest.mark.django_db
def test_no_payer_debtor():
    u1, u2 = user_recipe.make(_quantity=2)
    payment = Payment.objects.create(payer=u1, payment_value=Money(10))
    d2 = payment.add_debtor(u2, 1)
    d2.confirm()
    assert payment.all_debtors_confirmed
    update_user_debts()

    assert Debt.objects.get(user_1=u2, user_2=u1).debt_value.amount == 10
