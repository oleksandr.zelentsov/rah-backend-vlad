from django.urls import path

from payments.views import (
    PaymentListCreateView,
    PaymentCSVImportView,
    DebtorListCreateView,
    PaymentDetailView,
    DebtorDetailView,
    DebtorPickView,
    UnconfirmedDebtorView,
    DebtorConfirmView,
    DebtSourcesView
)

urlpatterns = [
    # payments
    path("", PaymentListCreateView.as_view(), name="payment_list"),
    path("<int:payment_id>/", PaymentDetailView.as_view(), name="payment_detail"),
    # debtors
    path(
        "<int:payment_id>/debtors/", DebtorListCreateView.as_view(), name="debtor_list"
    ),
    path(
        "<int:payment_id>/debtors/<int:debtor_id>",
        DebtorDetailView.as_view(),
        name="debtor_detail",
    ),
    path("pick-debtor/", DebtorPickView.as_view(), name="pick_debtor"),
    path(
        "unconfirmed-debtors/",
        UnconfirmedDebtorView.as_view(),
        name="unconfirmed_debtors",
    ),
    path("confirmed-debtors/", DebtSourcesView.as_view(), name="confirmed_debtors"),
    path(
        "unconfirmed-debtors/<int:debtor_id>/confirm/",
        DebtorConfirmView.as_view(),
        name="confirm_debtor",
    ),
    # other
    path("csv-import/", PaymentCSVImportView.as_view(), name="payment_csv_import"),
]
