from django.http import Http404
from django.utils.translation import ugettext
from rest_framework import serializers
from rest_framework.generics import (
    ListCreateAPIView,
    CreateAPIView,
    RetrieveUpdateDestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
)

from graphs.services import update_user_debts
from payments.models import Payment, Debtor
from payments.serializers import (
    PaymentsCSVImportSerializer,
    DebtorCreateSerializer,
    MyPaymentSerializer,
    DebtorDetailSerializer,
    DebtorPickSerializer,
    BaseDebtorSerializer,
    UnconfirmedDebtorSerializer,
    DebtSourcesSerializer
)


class PaymentListCreateView(ListCreateAPIView):
    serializer_class = MyPaymentSerializer

    def get_queryset(self):
        return Payment.objects.filter(payer=self.request.user).order_by("-payment_date")


class PaymentDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = MyPaymentSerializer

    def destroy(self, request, *args, **kwargs):
        if self.get_object().added_to_graph:
            raise serializers.ValidationError({
                "payment": ugettext("it's impossible to delete a fully confirmed payment")
            })

        return super().destroy(request, *args, **kwargs)

    def get_object(self):
        try:
            return Payment.objects.get(
                payer=self.request.user, id=self.kwargs["payment_id"]
            )
        except Payment.DoesNotExist:
            raise Http404


class DebtorListCreateView(ListCreateAPIView):
    serializer_class = DebtorCreateSerializer

    def get_queryset(self):
        return Debtor.objects.filter(
            payment__payer=self.request.user,
            payment=self.kwargs['payment_id'],
        )


class DebtSourcesView(RetrieveAPIView):
    serializer_class = DebtSourcesSerializer

    def get_object(self):
        positive, negative = Debtor.objects.get_debt_sources(self.request.user)
        return {
            "positive_debtors": positive,
            "negative_debtors": negative,
        }


class DebtorDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = DebtorDetailSerializer

    def destroy(self, request, *args, **kwargs):
        if self.get_object().payment.added_to_graph:
            raise serializers.ValidationError({
                "payment": ugettext("it's impossible to alter a fully confirmed payment"),
            })

        return super().destroy(request, *args, **kwargs)


class DebtorPickView(CreateAPIView):
    serializer_class = DebtorPickSerializer


class UnconfirmedDebtorView(ListAPIView):
    serializer_class = UnconfirmedDebtorSerializer

    def get_queryset(self):
        return Debtor.objects.get_confirmed_for_user(self.request.user, confirmed=False)


class DebtorConfirmView(RetrieveAPIView):
    serializer_class = BaseDebtorSerializer

    def get_object(self):
        obj: Debtor = Debtor.objects.get(pk=self.kwargs["debtor_id"])
        if not obj.confirmed:
            obj.confirm()
            if obj.payment.all_debtors_confirmed:
                update_user_debts()

        return obj


class PaymentCSVImportView(CreateAPIView):
    serializer_class = PaymentsCSVImportSerializer
