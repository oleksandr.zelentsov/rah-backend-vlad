import pytest

from accounts.models import User
from payments.serializers import MyPaymentSerializer
from utils.helpers import money_is_close
from utils.testing import FakeRequest

PAYMENT_CREATE_VALID_DATA = [
    (100, "lol"),
    (200, "300 - 100"),
    (5, "   didnt learn to put spaces correctly !      "),
]


@pytest.mark.django_db
@pytest.mark.parametrize("payment_value, description", PAYMENT_CREATE_VALID_DATA)
def test_payment_create_serializer_basic(payment_value, description, api_user: User):
    data = {"payment_value": payment_value, "description": description}

    serializer = MyPaymentSerializer(
        data=data, context=FakeRequest.build_context(api_user)
    )
    assert serializer.is_valid(raise_exception=False), serializer.errors
    payment = serializer.save()

    assert money_is_close(payment.payment_value, payment_value)
    assert payment.description == description.strip()
    assert payment.payer == api_user
    assert not payment.debtors.all().exists()
