# Generated by Django 2.2.1 on 2019-08-01 16:42

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_auto_20190730_2118'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='description',
            field=models.CharField(default='', max_length=30),
        ),
        migrations.AddField(
            model_name='payment',
            name='payment_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
