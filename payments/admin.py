# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.utils.translation import ugettext_lazy

from graphs.services import update_user_debts
from payments.models import Debtor, Payment


class PaymentAdmin(ModelAdmin):
    list_display = (
        "id",
        "payment_value",
        "payer",
        "real_value_coefficient",
        "all_debtors_confirmed",
        "num_debtors",
        "created",
    )
    list_filter = ("payment_value", "payer")
    date_hierarchy = "created"
    ordering = ("-created", )

    def num_debtors(self, instance) -> int:
        return instance.num_debtors

    num_debtors.short_description = ugettext_lazy("debtor count")

    def real_value_coefficient(self, instance: Payment) -> str:
        return "%s %s" % (str(round(instance.supposed_real_value_coefficient, 2)), instance.payment_value_currency)

    real_value_coefficient.short_description = ugettext_lazy("One relative point cost")

    def all_debtors_confirmed(self, instance) -> bool:
        return instance.all_debtors_confirmed

    all_debtors_confirmed.short_description = ugettext_lazy("All debtors confirmed")
    all_debtors_confirmed.boolean = True


def confirm_debtors(modeladmin, request, queryset):
    queryset.update(confirmed=True)
    update_user_debts()


confirm_debtors.short_description = ugettext_lazy("Confirm selected debtors")


class DebtorAdmin(ModelAdmin):
    list_display = (
        "id",
        "user",
        "relative_value",

        "amount",
        "one_point_cost",

        "confirmed",
    )
    list_filter = (
        "user",
        "confirmed",
    )

    actions = (
        confirm_debtors,
    )

    def amount(self, instance: Debtor):
        return instance.supposed_real_value

    @staticmethod
    def one_point_cost(instance: Debtor):
        return instance.payment.supposed_real_value_coefficient

    one_point_cost.short_description = ugettext_lazy("One point worth")


admin.site.register(Payment, PaymentAdmin)
admin.site.register(Debtor, DebtorAdmin)
