from decimal import Decimal
from typing import Union

from django.conf import settings
from django.utils.translation import ugettext
from djmoney.money import Money
from rest_framework.exceptions import ValidationError


def validate_payment_value_currency(value: str):
    if value not in settings.CURRENCIES:
        raise ValidationError(ugettext("This currency is not yet supported."))


def validate_payment_value(value: Union[float, Decimal, Money]):
    if isinstance(value, Money):
        value = value.amount

    if value < 0.01:
        raise ValidationError(ugettext("Payment value cannot be less than 0.01."))
