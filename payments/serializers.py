import csv
import logging
import tempfile
from decimal import Decimal, InvalidOperation
from typing import Optional, List, Tuple

from commonlib import attach_appropriate_names_for_fields
from django.db import transaction
from django.utils.translation import ugettext
from djmoney.contrib.django_rest_framework import MoneyField
from moneyed import CurrencyDoesNotExist
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField
from rest_framework.relations import PrimaryKeyRelatedField

from accounts.models import User
from accounts.serializers import BaseUserSerializer
from graphs.services import update_user_debts
from payments.models import Payment, Debtor
from payments.validators import validate_payment_value_currency, validate_payment_value
from utils.helpers import get_money, text_currency_to_float

DEBTOR_PICK_MIN_ID_LENGTH = 5

logger = logging.getLogger(__name__)


class BaseDebtorSerializer(serializers.ModelSerializer):
    real_value = MoneyField(16, 2, read_only=True)
    user = PrimaryKeyRelatedField(write_only=True, queryset=User.active_users)
    first_name = CharField(read_only=True, source="user.first_name")
    last_name = CharField(read_only=True, source="user.last_name")
    email = CharField(read_only=True, source="user.email")
    supposed_real_value = MoneyField(16, 2, read_only=True)
    supposed_real_value_currency = CharField(source="supposed_real_value.currency", read_only=True)

    class Meta:
        model = Debtor
        fields = (
            "payment",
            "user",
            "relative_value",
            "real_value",
            "confirmed",
            "last_name",
            "first_name",
            "email",
            "supposed_real_value",
            "supposed_real_value_currency",
        )


class UnconfirmedDebtorSerializer(BaseDebtorSerializer):
    first_name = CharField(read_only=True, source="payment.payer.first_name")
    last_name = CharField(read_only=True, source="payment.payer.last_name")
    email = CharField(read_only=True, source="payment.payer.email")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["payment"] = BasePaymentSerializer()

    class Meta:
        model = Debtor
        fields = ("id", "first_name", "last_name", "email", "payment", "real_value", "supposed_real_value", "supposed_real_value_currency")


class InPaymentDebtorSerializer(BaseDebtorSerializer):
    """A debtor, but with no payment. For hierarchical display, payments -> debtors."""

    class Meta:
        model = Debtor
        fields = ("user", "relative_value", "real_value", "confirmed")


class DebtorCreateSerializer(BaseDebtorSerializer):
    """
    Exists to allow for creation and listing of debtors in Payment API.
    """
    user = PrimaryKeyRelatedField(write_only=True, queryset=User.active_users)
    first_name = CharField(read_only=True, source="user.first_name")
    last_name = CharField(read_only=True, source="user.last_name")
    email = CharField(read_only=True, source="user.email")
    real_value = MoneyField(16, 2, read_only=True)
    supposed_real_value = MoneyField(16, 2, read_only=True)

    def create(self, validated_data):
        payment_id = self.context['request'].parser_context['kwargs']["payment_id"]
        user = self.context['request'].user
        payment = validated_data['payment'] = Payment.objects.get(
            payer=user,
            pk=payment_id,
        )
        if payment.added_to_graph:
            raise serializers.ValidationError({
                "user": [
                    ugettext("it's impossible to change the payment once all the debtors have confirmed the debt")
                ]
            })

        existing_debtor = Debtor.objects.filter(user=validated_data['user'].id, payment=payment_id, )
        if existing_debtor.exists():
            raise serializers.ValidationError(
                {
                    "user": [
                        ugettext(
                            f"the debtor already exists and goes by the name {str(existing_debtor.first().user)}"
                        )
                    ]
                }
            )

        if validated_data['user'] == self.context['request'].user:  # autoconfirming the self debtor
            validated_data['confirmed'] = True

        return super().create(validated_data)

    class Meta:
        model = Debtor
        fields = ("id", "user", "first_name", "last_name", "email", "relative_value", "real_value", "supposed_real_value", "confirmed")
        extra_kwargs = {"confirmed": {"read_only": True}, "real_value": {"read_only": True}, "supposed_real_value": {"read_only": True}}


class DebtorDetailSerializer(BaseDebtorSerializer):
    """
    Serves to display and edit detailed information on debtors in Debtor API.
    """

    payment = serializers.PrimaryKeyRelatedField(
        queryset=Payment.objects.all()
    )  # todo change queryset

    class Meta:
        model = Debtor
        fields = (
            "payment",
            "user",
            "relative_value",
            "real_value",
            "percentage",
            "confirmed",
        )
        extra_kwargs = {
            "payment": {"read_only": True},
            "user": {"read_only": True},
            "real_value": {"read_only": True},
            "percentage": {"read_only": True},
            "confirmed": {"read_only": True},
        }


class BasePaymentSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.fields.get('payment_value') is not None:
            self.fields['payment_value'].validators += [
                validate_payment_value,
            ]

        if self.fields.get('payment_value_currency') is not None:
            self.fields['payment_value_currency'].validators += [
                validate_payment_value_currency,
            ]

    def to_internal_value(self, data):
        try:
            return super().to_internal_value(data)
        except CurrencyDoesNotExist:
            raise ValidationError({"payment_value_currency": ["No such currency."]})

    def create(self, validated_data):
        validated_data = dict(validated_data, payer=self.context["request"].user)
        payment = super().create(validated_data)
        return payment

    class Meta:
        model = Payment
        fields = ("id", "payer", "payment_value", "payment_value_currency", "description", "payment_date")


class MyPaymentSerializer(BasePaymentSerializer):
    """Payment object with no payer user information (assumption: it's mine!)."""

    class Meta:
        model = Payment
        fields = ("id", "payment_value", "payment_value_currency", "description", "payment_date")


class DebtorPickSerializer(serializers.Serializer):
    """Responsible for picking a debtor user during debtor creation process."""

    identifier = serializers.CharField(write_only=True)
    candidates = BaseUserSerializer(many=True, read_only=True)

    @staticmethod
    def validate_identifier(value):
        value = value.strip()
        if not value or len(value) < DEBTOR_PICK_MIN_ID_LENGTH:
            raise serializers.ValidationError(
                ugettext(f"too short of a query string, must be {DEBTOR_PICK_MIN_ID_LENGTH} long non-space")
            )
        return value

    def create(self, validated_data):
        identifier = validated_data["identifier"].strip()
        found_users = list(User.objects.find_by_string(identifier))
        if not found_users:
            raise serializers.ValidationError({"identifier": [ugettext("no user found")]}, code=404)

        return {"candidates": found_users}

    class Meta:
        fields = ("identifier", "candidates")


class PaymentsCSVImportSerializer(BasePaymentSerializer):
    import_file = serializers.FileField(write_only=True)

    imported_payments = BasePaymentSerializer(many=True, read_only=True)
    imported_debtors = BaseDebtorSerializer(many=True, read_only=True)

    def create(self, validated_data):
        user = self.context["request"].user
        import_file = validated_data["import_file"]
        with tempfile.NamedTemporaryFile(mode="w", delete=False) as t_fp:
            filename = t_fp.name
            t_fp.write(import_file.read().decode("utf-8"))

        with transaction.atomic():
            payments, debtors = self.from_csv(filename=filename, user=user)

        update_user_debts()

        return {"imported_payments": payments, "imported_debtors": debtors}

    @classmethod
    def from_csv(
        cls, user: User, filename: str = None, fp=None, **parser_args
    ) -> Tuple[List[Payment], List[Debtor]]:
        if filename is not None and fp is None:
            with open(filename, "r") as fp:
                reader = csv.DictReader(fp, **parser_args)
                rows = [dict(row) for row in reader]
        elif filename is None and fp is not None:
            reader = csv.DictReader(fp, **parser_args)
            rows = [dict(row) for row in reader]
        else:
            raise ValueError("only one of the arguments should be filled")

        if user is None:
            raise ValueError("no user which imports the data")

        rows = attach_appropriate_names_for_fields(rows)
        payments = []
        all_debtors = []
        for row in rows:
            payment, debtors = cls._from_csv_row(row, user=user)
            if payment is not None:
                payments.append(payment)

            all_debtors += debtors

        return payments, all_debtors

    @classmethod
    def _from_csv_row(
        cls, row: dict, user: User
    ) -> Tuple[Optional[Payment], List[Debtor]]:
        try:
            total_price: str = row.pop("total_price")
        except ValueError:
            logger.error("no total price of payment in the table")
            return None, []

        try:
            currency: str = row.pop("currency")
        except ValueError:
            logger.error("no currency in the table")
            return None, []

        try:
            total_price: Decimal = get_money(total_price, currency)
        except (TypeError, ValueError, InvalidOperation) as e:
            row["total_price"] = total_price
            logger.error(
                "error with total_price value: %r, skipping row %r", total_price, row
            )
            logger.exception(e)
            return None, []

        confirmed = user.is_superuser
        who_paid: str = row.pop("who_paid")
        who_paid: User = User.objects.find_by_string_or_create_fictional(
            who_paid, author_user=user, return_tuple=False
        )
        description: str = row.pop("description")
        metadata_fields = [field for field in row.keys() if field.startswith(":")]
        metadata = {key[1:]: row.pop(key) for key in metadata_fields}
        payment = Payment.objects.create(
            payment_value=total_price,
            payer=who_paid,
            metadata=metadata,
            from_csv=True,
            imported_by=user,
            added_to_graph=False,
            description=description,
        )
        debtors = []
        try:
            for field in row:
                relative_value_text = row[field] or "0"
                relative_value = text_currency_to_float(relative_value_text)
                if relative_value == 0:
                    continue  # skipping empty debtor

                debtor_user = User.objects.find_by_string_or_create_fictional(
                    field, author_user=user, return_tuple=False
                )
                debtors.append(
                    Debtor.objects.create(
                        payment=payment,
                        user=debtor_user,
                        relative_value=relative_value,
                        confirmed=confirmed,  # payment debts are not confirmed by default
                        from_csv=True,
                        imported_by=user,
                    )
                )
        except (TypeError, ValueError) as e:
            logger.error("error with row: %r", row)
            logger.exception(e)
            return None, []

        return payment, debtors

    class Meta:
        model = Payment
        fields = ("import_file", "imported_payments", "imported_debtors")


class DebtSourcesSerializer(serializers.Serializer):
    positive_debtors = BaseDebtorSerializer(many=True)
    negative_debtors = UnconfirmedDebtorSerializer(many=True)

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError

    class Meta:
        fields = (
            "positive_debtors",
            "negative_debtors",
        )
