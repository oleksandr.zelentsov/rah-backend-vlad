# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
from collections import defaultdict
from contextlib import contextmanager
from copy import copy
from decimal import Decimal
from typing import Dict, Union, Tuple, Optional

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.core.validators import MinValueValidator
from django.db import models, transaction
from django.db.models import QuerySet, Q, F, OuterRef, Subquery
from django.utils.functional import cached_property
from django.utils.translation import ugettext
from djmoney.models.fields import MoneyField
from djmoney.money import Money

from accounts.models import User
from utils.django.functions import SQCount
from utils.django.models import ArchivedModel
from utils.helpers import get_money

logger = logging.getLogger(__name__)


class PaymentManager(models.Manager):
    def get_queryset(self):
        debtor_objs_sq = SQCount(
            Debtor.objects.filter(payment_id=OuterRef('id'), confirmed=True).values('id').distinct('id'),
            output_field=models.IntegerField(),
        )
        return super().get_queryset().annotate(
            num_debtors=models.Count("debtors", distinct=True),
            num_confirmed_debtors=debtor_objs_sq,
        )

    def to_be_added_to_graph(self):
        return self.get_queryset().filter(
            added_to_graph=False,
            num_debtors=F('num_confirmed_debtors'),
        )


class Payment(ArchivedModel):
    objects = PaymentManager()
    payer = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="performed_payments"
    )
    payment_value = MoneyField(
        max_digits=19,
        decimal_places=4,
        default_currency=settings.CURRENCIES[0],
        currency_choices=settings.CURRENCY_CHOICES,
    )
    debtors = models.ManyToManyField(
        "accounts.User", through="Debtor", through_fields=("payment", "user")
    )
    added_to_graph = models.BooleanField(
        default=False,
        verbose_name=ugettext("was processed within graph minimization process and converted to Debts"),
    )
    description = models.CharField(max_length=300)
    payment_date = models.DateTimeField(auto_now_add=True)
    from_csv = models.BooleanField(default=False)
    imported_by = models.ForeignKey(
        "accounts.User",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="imported_payments",
    )
    metadata = JSONField(default=dict, null=True, blank=True)

    def __repr__(self):
        result = f"{self.payment_value}: {self.payer or ''}"
        debtor_strs = []
        for debtor in Debtor.objects.filter(payment=self):
            debtor_strs.append(str(debtor))

        if debtor_strs:
            return result + " -> " + " | ".join(debtor_strs)
        else:
            return result

    def __str__(self):
        return repr(self)

    def involved_users(self) -> QuerySet:
        return User.objects.filter(
            Q(pk=self.payer_id) | Q(pk__in=self.debtors.all().values("id"))
        )

    def confirmed_debtors(self):
        return Debtor.objects.filter(payment=self, confirmed=True).prefetch_related(
            "user"
        )

    @cached_property
    def all_debtors_confirmed(self) -> bool:
        confirmed_debtors = self.confirmed_debtors()
        all_debtors = Debtor.objects.filter(payment=self)
        payer_debtor = self.payer_debtor
        empty_debtor_set = {payer_debtor} if payer_debtor else set()
        return set(confirmed_debtors) == set(all_debtors) and set(all_debtors) != empty_debtor_set

    def people_debts(self) -> Dict[User, float]:
        return {
            debtor.user: debtor.relative_value for debtor in self.confirmed_debtors()
        }

    def calculate(self) -> Dict:
        """Get a dict that has all the debtors as keys and their r-values as values."""
        total_count = sum([v for _, v in self.people_debts().items()])
        if total_count == 0:
            return defaultdict(int)

        one_price = self.payment_value / total_count
        result = {}
        for person, count in self.people_debts().items():
            logger.debug(
                "%i self.payment_value_currency %s",
                self.id,
                self.payment_value_currency,
            )
            person_price = get_money(count * one_price, self.payment_value_currency)
            if person_price != 0:  # TODO check if money_is_close is required
                result[person] = person_price

        return result

    @cached_property
    def real_value_coefficient(self):
        """A ratio between the actual value of spent money in payment and the sum of relative values of all debtors."""
        value_sum = self.debt_relative_value_sum
        if value_sum == 0:
            return 0

        return self.payment_value / Money(value_sum, self.payment_value_currency)

    @cached_property
    def supposed_real_value_coefficient(self):
        value_sum = self.debt_relative_value_sum_supposed
        if value_sum == 0:
            return 0

        return self.payment_value / value_sum

    def relative_value_to_real_value(self, relative_value: Union[int, Decimal], supposed=False) -> Money:
        """Convert relative value of debt to a real value for this payment."""
        if supposed:
            rvc = self.supposed_real_value_coefficient
        else:
            rvc = self.real_value_coefficient

        if isinstance(rvc, Money):
            rvc = rvc.amount

        return Money(rvc * Decimal(relative_value), self.payment_value.currency)

    def real_value_to_relative_value(self, real_value: Money, supposed=False) -> Decimal:
        """Convert a real debt value to a relative value for this payment."""
        if supposed:
            rvc = self.supposed_real_value_coefficient
        else:
            rvc = self.real_value_coefficient

        return Decimal(real_value.amount) / Decimal(rvc)

    @cached_property
    def payer_debtor(self) -> Optional["Debtor"]:
        """A Debtor instance of the person, who does not owe anything in this payment."""
        try:
            return self.debtor_set.get(user=self.payer)
        except Debtor.DoesNotExist:
            return None

    @cached_property
    def debt_relative_value_sum(self) -> float:
        """Sum of all relative values of all confirmed debtors."""
        return sum(
            [
                debtor.relative_value
                for debtor in self.debtor_set.all()
                if debtor.confirmed
            ]
        )

    @cached_property
    def debt_relative_value_sum_supposed(self) -> float:
        """Sum of all relative values of all confirmed debtors."""
        return sum(
            [
                debtor.relative_value
                for debtor in self.debtor_set.all()
            ]
        )

    @property
    def non_payer_relative_debt_value_sum(self):  # TODO test
        return self.debt_relative_value_sum - self.payer_relative_debt

    @property
    def non_payer_real_debt_value_sum(self):  # TODO test
        return self.relative_value_to_real_value(self.non_payer_relative_debt_value_sum)

    @property
    def payer_relative_debt(self):  # TODO test
        """Which part of the whole payment is actually spent by the payer."""
        return self.payer_debtor and self.payer_debtor.relative_value

    @property
    def payer_real_debt(self):  # TODO test
        """How much money out of the whole payment is actually spent by the payer."""
        return self.relative_value_to_real_value(self.payer_relative_debt)

    def add_debtor(
        self, user: User, relative_value: float = 0, save_=True, **debtor_kwargs
    ):
        debtor = Debtor.objects.create(
            user=user,
            payment=self,
            relative_value=relative_value,
            **debtor_kwargs
        )
        # if save_:
        #     debtor.save()

        return debtor


class DebtorManager(models.Manager):
    def get_payer_debtors(self):
        return super().get_queryset().annotate(
            payer_id=Subquery(
                Payment.objects.filter(pk=OuterRef("payment_id")).values('payer_id')[:1],
                output_field=models.BigIntegerField(),
            ),
        ).annotate(
            is_payer_debtor=models.Case(
                models.When(payer_id=F("user_id"), then=True),
                default=False,
                output_field=models.BooleanField(),
            )
        )

    def get_debt_sources(self, user) -> Tuple[QuerySet, QuerySet]:
        """
        Returns a tuple where the first element tells you
        how did you get all your positive debts and the
        second elements - where did you get all your negative debts.
        """
        return self.get_payer_debtors().filter(
            payment__payer=user,
            confirmed=True,
            is_payer_debtor=False,
        ), self.get_confirmed_for_user(user)

    def get_confirmed_for_user(self, user, confirmed: bool = True):
        return self.get_payer_debtors().filter(
            user=user,
            confirmed=confirmed,
            is_payer_debtor=False,
        ).order_by("-payment__created")


class Debtor(models.Model):
    objects = DebtorManager()
    payment = models.ForeignKey(Payment, on_delete=models.CASCADE)
    user = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="as_debtor"
    )
    relative_value = models.DecimalField(
        decimal_places=2,
        max_digits=16,
        validators=[MinValueValidator(0.01, ugettext("debt value cannot be zero or less"))]
    )
    confirmed = models.BooleanField(default=False)
    from_csv = models.BooleanField(default=False)
    imported_by = models.ForeignKey(
        "accounts.User",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="imported_debtors",
    )

    def confirm(self, save_=True):
        self.confirmed = True
        if save_:
            self.save()

    def disapprove(self, save_=True):
        self.confirmed = False
        if save_:
            self.save()

    @cached_property
    def real_value(self):
        """A real debtor value. Only takes into account confirmed debts."""
        if not self.confirmed:
            return 0

        return self.payment.relative_value_to_real_value(self.relative_value)

    @cached_property
    def supposed_real_value(self):
        return self.payment.relative_value_to_real_value(self.relative_value, supposed=True)

    @property
    def percentage(self):
        return round(self.real_value / self.payment.payment_value * 100, 2)

    def __repr__(self):
        return f'{self.user}: {self.real_value or self.supposed_real_value}{", confirmed" if self.confirmed else ""}'

    __str__ = __repr__

    class Meta:
        unique_together = ("payment", "user")


@contextmanager
def suppose_confirmed_debtor(debtor: Debtor):
    with transaction.atomic():
        prev = copy(debtor.confirmed)
        debtor.confirmed = True
        debtor.save()
        yield
        debtor.confirmed = prev
        debtor.save()
