# Rah

## Prerequisites

1. Make sure you have Python 3.7 installed.
2. [Windows only] Make sure you have [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) installed. [Ubuntu](https://docs.microsoft.com/en-us/windows/wsl/install-manual) is recommended. On all systems, please have `bash` available and run the installation steps inside it.
3. Generate a pair of keys using [Generating a pair of SSH keys](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair). You may leave passphrase empty. Add the public key to [your Gitlab account](https://gitlab.com/profile/keys).
4. Install [Docker](https://docs.docker.com/docker-for-windows/install/).

## Installation
```bash
git clone git@gitlab.com:rah-2-0/rah-backend.git
cd rah-backend
docker-compose -f docker-compose-local.yml up -d
python3.7 -m venv env
env/bin/activate

python3.7 -m pip install -r requirements.txt -r requirements-dev.txt
(
printf <<EOF
#!/bin/bash

local PY_FILES=`git diff --cached --name-only --diff-filter=d | egrep '\.pyi?$'`
if [[ ! -z "\$PY_FILES" ]]; then
    black \${PY_FILES}
    git add \${PY_FILES}
fi 
EOF
) > .git/hooks/precommit

cp rah/local_settings.template.py rah/local_settings.py
docker-compose -f docker-compose-local.yml up -d
python3.7 manage.py migrate
```
## Running tests

After the installation steps, inside the virtualenv, you should be able to run `pytest` with no arguments to run tests.