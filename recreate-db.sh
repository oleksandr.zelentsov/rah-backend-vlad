#!/usr/bin/env bash

REQUIREMENTS="requirements.txt requirements-dev.txt"
DB_VOLUME_NAME="rah-backend_db-volume"
DEFAULT_DOCKER_COMPOSE_CONFIG="docker-compose-local.yml"
MANAGE_PY='manage.py'
PYTHON='python3.7'

# fail on error, fail pipe on error
set -ef -o pipefail

function usage() {
    if [[ -z $1 ]]; then  # use custom return code (pass -1 not to exit)
        return_code=0
    else
        return_code=$1
    fi

    if [[ ! -z $2 ]]; then  # print custom error message
        echo $2 >&2
        echo
    fi

    echo "Usage: $0 [OPTION [OPTION...]] [DOCKER_COMPOSE_CONFIG] [OPTION [OPTION...]]" >&2
    echo >&2
    echo "Note: DOCKER_COMPOSE_CONFIG is ${DEFAULT_DOCKER_COMPOSE_CONFIG}, by default."  >&2
    echo >&2
    echo "Options:" >&2
    echo "--help               show help this message and quit" >&2
#    echo "--seeds              create test user data" >&2
    echo "--no-requirements    do not run requirements installation" >&2
    echo "--migration-retry    retry migrations on fail" >&2
    echo '--dry-run            only print commands that will be executed' >&2
    echo >&2
    if [[ ${return_code} != -1 ]]; then
        exit ${return_code}
    fi
}

while [[ ! -z "$@" ]]; do
    case "$1" in
        "--help")
        usage
        shift
        ;;

#        "--seeds")
#        CREATE_SEEDS=1
#        shift
#        ;;

        "--dry-run")
        DRY_RUN=1
        if [[ ! -z ${DRY_RUN} ]]; then
            CMD='echo'
        else
            CMD=''
        fi
        shift
        ;;

        "--no-requirements")
        NO_REQUIREMENTS=1
        shift
        ;;

        "--migration-retry")
        MIGRATION_RETRY=1
        shift
        ;;

        *.yml)
        if [[ ! -z "${COMPOSE_YML}" ]]; then
            usage 3 "Can't have 2 different Docker configuration files: ${COMPOSE_YML}, $1"
        fi
        COMPOSE_YML=$1
        shift
        ;;

        *)
        usage 2 "Can't parse argument: ${1}"
        shift
    esac
done

# check for a non-default docker config
if [[ -z ${COMPOSE_YML} ]]; then
    COMPOSE_YML=${DEFAULT_DOCKER_COMPOSE_CONFIG}
fi

# see if we are doing it correctly, not to confuse non-python devs
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo "Error: not running in virtualenv!" >&2
    echo "Please, enter Python virtual environment, or ask your colleague how to do that :)" >&2
    exit 1
fi

if [[ ! -f ${COMPOSE_YML} ]]; then
    usage 1 "File not found: ${COMPOSE_YML}"
fi
echo "*** Bringing them containers down..."
${CMD} docker-compose -f ${COMPOSE_YML} down --remove-orphans

# throw away the volume with DB
if docker volume ls | grep "${DB_VOLUME_NAME}" >/dev/null; then
    echo "*** Deleting the volume with DB..."
    ${CMD} docker volume rm ${DB_VOLUME_NAME}
fi

# recreate the volume (should download etc etc)
echo "*** Recreating the volume, bringing the containers up..."
${CMD} docker-compose -f ${COMPOSE_YML} up -d

if [[ -z ${NO_REQUIREMENTS} ]]; then
    echo "*** Installing Python requirements..."
    ${CMD} ${PYTHON} -m pip install $(echo "$REQUIREMENTS" | sed 's/[^ ]* */-r &/g')
fi

echo "*** Running migrations..."
set +e
${CMD} python ${MANAGE_PY} migrate
while [[ $? != 0 ]] && [[ ${MIGRATION_RETRY} == 1 ]]; do
    sleep 5
    ${CMD} python ${MANAGE_PY} migrate
done
set -e

#if [[ ! -z ${CREATE_SEEDS} ]]; then
#    echo "*** Creating super user..."
#     ${CMD} ${PYTHON} ${MANAGE_PY} seeds
#fi

echo "*** Recreating the database was successful!"
