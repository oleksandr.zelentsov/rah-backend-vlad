from logging import getLogger

from django.core.management import BaseCommand
from django.db import IntegrityError
from faker import Faker

from accounts.models import User

DEFAULT_PASSWORD = '12341234'
faker = Faker(['pl-PL'])


logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'Creates data for testing.'

    def handle(self, *args, **options):
        for i in range(10):
            first_name, last_name = faker.first_name(), faker.last_name()
            username = f"{first_name[0].lower()}{last_name.lower()}{i+1}{faker.numerify(text='##')}".format_map({
                "ą": "a",
                "ę": "e",
                "ń": "n",
                "ó": "o",
                "ł": "l",
                "ć": "c",
                "ż": "z",
                "ź": "z",
            })
            email = f"test{i+1}@example.com"
            try:
                user = User(
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    username=username,
                    is_active=True,
                )
                user.set_password(DEFAULT_PASSWORD)
                user.save()
                logger.debug("created user %s", email)
            except IntegrityError:
                logger.warning("the user %s already exists, skipping", email)
