from decimal import Decimal


def integrity_info_to_graph_integrity(integrity, users, currency):
    return {
        users[user_idx]: Decimal(integrity_value)
        for user_idx, integrity_value in integrity.items()
    }
