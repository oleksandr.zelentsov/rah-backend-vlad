import io
import logging
import os
from collections import defaultdict
from decimal import Decimal
from itertools import combinations
from typing import Collection, Dict, Sequence, Union, Generator, Tuple, Iterable, List, Callable

import networkx as nx
from django.db import transaction
from django.db.models import QuerySet, Q
from djmoney.money import Money
from matplotlib import pyplot as plt

from accounts.models import User
from debts.models import Debt
from graphs.errors import EmptyGraphError, NoCurrencyGraphError, TooManyCurrenciesGraphError
from payments.models import Payment
from payments_debts.services import PaymentService
from utils.helpers import get_money, money_is_close

logger = logging.getLogger(__name__)

GRAPH_DEBT_VALUE_KEY = "debt"


class GraphService:
    """
    A data structure used to reflect a debt graph,
    which means there is a number of assumptions put on it:
    - the graph does not have parallel edges
    - the graph does not have self-loops (the underlying nx.DiGraph data structure forbids it)
    - edges universally have positive non-zero weights of class Money,
     which mean the amount of money somebody owes.
    - there can be only 1 currency in the whole graph
    """

    def __init__(self, graph: nx.DiGraph):
        if not graph:
            raise EmptyGraphError()

        self.currency: str = self.get_graph_currency(graph)
        self.graph: nx.DiGraph = graph

    @property
    def is_minimized(self):
        who_owes = set()
        who_is_owed = set()

        for user_1, user_2 in self.graph.edges:
            who_owes.add(user_1)
            who_is_owed.add(user_2)

        return (who_owes & who_is_owed) == set()

    @classmethod
    def from_payments_list(
        cls,
        payments: Union[Collection[Payment], QuerySet],
        users: Union[Collection[User], QuerySet] = None,
    ):
        """
        Use this only when you want to convert payments to debts.
        If you want to generate the graph from existing debts, use GraphService.from_debts_list().
        """
        users = users or User.objects.all()
        debts = PaymentService.generate_debts_from_payment_history(payments, users)
        return cls.from_debts_list(debts)

    @classmethod
    def get_graph_currency(cls, graph: nx.DiGraph):
        currencies = cls.get_graph_currencies(graph)
        currency_count = len(set(currencies))
        if currency_count == 0:
            raise NoCurrencyGraphError()
        elif currency_count == 1:
            return list(currencies)[0]
        else:
            raise TooManyCurrenciesGraphError()

    @staticmethod
    def get_graph_currencies(graph):
        return [graph.edges[u1, u2]["debt"].currency for u1, u2 in list(graph.edges)]

    @classmethod
    def from_all_debts(cls) -> "GraphService":
        return cls.from_debts_list(Debt.objects.all())

    @classmethod
    def from_debts_list(cls, debts: "Iterable[Debt]"):
        all_currencies = set([debt.debt_value.currency for debt in debts])
        assert 0 <= len(all_currencies) <= 1

        graph = nx.DiGraph()
        for debt in debts:
            graph.add_edge(
                debt.user_1, debt.user_2, **{GRAPH_DEBT_VALUE_KEY: debt.debt_value}
            )

        return cls(graph)

    def to_debts_list(self, **debt_kwargs):
        debts = []
        for u1, u2, d12 in self.walk_through_edges():
            if d12:
                debt = Debt(user_1=u1, user_2=u2, debt_value=d12, **debt_kwargs)
                debts.append(debt)

        return debts

    @classmethod
    def payments_to_debts(cls, payments, users=None):
        return cls.from_payments_list(payments, users).to_debts_list()

    def __add__(self, other):
        return self.merge_graphs(other)

    def merge_graphs(self, other_gs: "GraphService"):
        if not isinstance(other_gs, GraphService):
            raise TypeError(
                f"cannot merge a debt graph with {other_gs.__class__.__name__}"
            )

        if self.currency != other_gs.currency:
            raise TypeError("impossible to merge graphs with different currencies")

        graph = self.graph.copy()
        result_gs = self.__class__(graph)

        for u1, u2, d12 in other_gs.walk_through_edges():
            if money_is_close(d12, self.zero):
                continue

            result_gs.add_debt(u1, u2, d12)

        return result_gs

    def walk_through_edges(self) -> Generator[Tuple[User, User, Money], None, None]:
        """Produces triples of (who_owes, who_is_owed, how_much)."""
        return self.graph.edges(data=GRAPH_DEBT_VALUE_KEY, default=self.zero)

    def get_debt_value(self, user_1, user_2):
        try:
            edge = self.graph.edges[user_1, user_2]
        except KeyError:
            return self.zero
        else:
            return edge[GRAPH_DEBT_VALUE_KEY]

    def add_debt(self, u1: User, u2: User, d12: Money):
        if money_is_close(d12, self.zero):
            return None

        try:
            edge = self.graph.edges[u1, u2]
        except KeyError:
            self.graph.add_edge(u1, u2, **{GRAPH_DEBT_VALUE_KEY: d12})
        else:
            edge[GRAPH_DEBT_VALUE_KEY] += d12

        return self.graph.edges[u1, u2]

    @property
    def zero(self) -> Money:
        return Money(0, currency=self.currency)

    def resolve_parallel_edges(self):  # TODO test
        for n1, n2 in combinations(self.graph.nodes, 2):
            try:
                edge = self.graph.edges[n1, n2]
                reverse_edge = self.graph.edges[n2, n1]
            except KeyError:
                continue  # means there is one edge there

            d12 = edge[GRAPH_DEBT_VALUE_KEY]
            d21 = reverse_edge[GRAPH_DEBT_VALUE_KEY]
            diff = d12 - d21

            if money_is_close(diff, 0):
                self.graph.remove_edges_from([(n1, n2), (n2, n1)])
            elif diff > self.zero:  # d12 is greater
                self.graph.remove_edge(n2, n1)
                edge[GRAPH_DEBT_VALUE_KEY] -= d21
            else:  # d21 is greater
                self.graph.remove_edge(n1, n2)
                reverse_edge[GRAPH_DEBT_VALUE_KEY] -= d12

    def minimize_debt_graph(self):
        if self.is_minimized:
            logger.info("skipping minimization, graph already minimal")
            return

        self.resolve_parallel_edges()

        graph = self.graph.copy()
        work_done = True  # just a setup
        passes = 0
        while work_done:
            work_done = False
            for p1, p2, d12 in graph.edges(data=GRAPH_DEBT_VALUE_KEY, default=0):
                for _, p3, d23 in graph.edges(
                    [p2], data=GRAPH_DEBT_VALUE_KEY, default=0
                ):
                    points_are_not_unique = p1 == p2 or p2 == p3 or p1 == p3
                    zero_debts = all([money_is_close(d, 0) for d in [d12, d23]])
                    is_wrong_condition = zero_debts or points_are_not_unique
                    if is_wrong_condition:
                        continue

                    work_done = True
                    if money_is_close(d12, d23):
                        self._method1(d12, graph, p1, p2, p3)
                    elif d12 > d23:
                        self._method2(d12, d23, graph, p1, p2, p3)
                    elif d12 < d23:
                        self._method3(d12, d23, graph, p1, p2, p3)
                    else:
                        raise ArithmeticError(f"numbers {d12} and {d23} are not following >, <, or = rule.")

                    if work_done:
                        break
                if work_done:
                    break

            passes += 1

        assert self.are_graphs_equivalent(
            self.graph, graph
        ), self.get_integrity_difference(self.graph, graph)
        logger.info(f"finished graph minimization in {passes} passes")
        self.graph = graph

    @classmethod
    def are_graphs_equivalent(
        cls, graph_complex: nx.DiGraph, graph_simplified: nx.DiGraph
    ):
        try:
            comp_currency = cls.get_graph_currency(graph_complex)
        except NoCurrencyGraphError:
            comp_currency = None

        try:
            simple_currency = cls.get_graph_currency(graph_simplified)
        except NoCurrencyGraphError:
            simple_currency = None

        assert comp_currency == simple_currency
        complex_graph_integrity = cls.get_graph_integrity(graph_complex)
        simplified_graph_integrity = cls.get_graph_integrity(
            graph_simplified
        )
        return complex_graph_integrity == simplified_graph_integrity

    @classmethod
    def get_integrity_difference(
        cls, graph_1: nx.DiGraph, graph_2: nx.DiGraph
    ):
        comp_currency = cls.get_graph_currency(graph_1)
        simple_currency = cls.get_graph_currency(graph_2)
        assert comp_currency == simple_currency
        complex_graph_integrity = {
            (k, v) for k, v in cls.get_graph_integrity(graph_1).items()
        }
        simplified_graph_integrity = {
            (k, v) for k, v in cls.get_graph_integrity(graph_2).items()
        }
        return (
            complex_graph_integrity - simplified_graph_integrity,
            simplified_graph_integrity - complex_graph_integrity,
        )

    @property
    def graph_integrity(self):
        return self.get_graph_integrity(self.graph)

    @staticmethod
    def get_graph_integrity(graph: nx.DiGraph) -> Dict[User, Decimal]:
        zero = Decimal("0")
        integrity: Dict[User, Decimal] = defaultdict(lambda: zero)

        for p1, p2, debt in graph.edges.data(GRAPH_DEBT_VALUE_KEY, default=zero):
            amount = debt.amount
            integrity[p1] = get_money(integrity[p1] - amount)
            integrity[p2] = get_money(integrity[p2] + amount)

        return {k: v for k, v in integrity.items() if v != 0}

    def _method1(self, d12, graph, p1, p2, p3):
        logger.debug(f"(1) simplifying ({p1} -> {p2} -> {p3}) to ({p1} -> {p3})")
        graph.remove_edge(p1, p2)
        graph.remove_edge(p2, p3)
        if graph.has_edge(p1, p3):
            new_debt = d12 + graph.edges[p1, p3][GRAPH_DEBT_VALUE_KEY]
            graph.remove_edge(p1, p3)
        else:
            new_debt = d12

        graph.add_edge(p1, p3, **{GRAPH_DEBT_VALUE_KEY: new_debt})

    def _method2(self, d12, d23, graph, p1, p2, p3):
        logger.debug(
            f"(2) simplifying ({p1} -> {p2} -> {p3}) to ({p1} -> {p2}), ({p1} -> {p3})"
        )
        graph.remove_edge(p1, p2)
        graph.remove_edge(p2, p3)
        if graph.has_edge(p1, p3):
            new_debt = d23 + graph.edges[p1, p3][GRAPH_DEBT_VALUE_KEY]
            graph.remove_edge(p1, p3)
        else:
            new_debt = d23

        graph.add_edge(
            p1,
            p2,
            **{GRAPH_DEBT_VALUE_KEY: d12 - d23},
        )
        graph.add_edge(p1, p3, **{GRAPH_DEBT_VALUE_KEY: new_debt})

    def _method3(self, d12, d23, graph, p1, p2, p3):
        logger.debug(
            f"(3) simplifying ({p1} -> {p2} -> {p3}) to ({p1} -> {p3}), ({p2} -> {p3})"
        )
        graph.remove_edge(p1, p2)
        graph.remove_edge(p2, p3)
        if graph.has_edge(p1, p3):
            new_debt = d12 + graph.edges[p1, p3][GRAPH_DEBT_VALUE_KEY]
            graph.remove_edge(p1, p3)
        else:
            new_debt = d12

        graph.add_edge(p1, p3, **{GRAPH_DEBT_VALUE_KEY: new_debt})
        graph.add_edge(
            p2,
            p3,
            **{GRAPH_DEBT_VALUE_KEY: d23 - d12},
        )

    def plot_one_to_all(
        self,
        user: User,
        *args,
        **kwargs
    ):
        """
        Plots all the debts related to 1 user.
        """
        users = {user}
        for user_1, user_2, _ in self.walk_through_edges():
            if user == user_1:
                users.add(user_2)
                continue

            if user == user_2:
                users.add(user_1)
                continue

        if not users:
            raise ValueError("no users given")

        return self.plot(list(users), *args, **kwargs)

    def plot(
        self,
        users_to_plot: Sequence[User] = None,
        *,
        file_extension=None,
        output_filename: str = None,
        return_file_content=False,
        user_to_str: Callable = str
    ):
        graph = self.graph
        if users_to_plot:
            graph = self.graph.subgraph(users_to_plot)

        positioning = nx.circular_layout(graph)
        nx.draw_networkx_nodes(
            graph, positioning, cmap=plt.get_cmap("jet"), node_size=320
        )
        nx.draw_networkx_edge_labels(
            graph,
            positioning,
            edge_labels={
                (u1, u2): round(data[GRAPH_DEBT_VALUE_KEY].amount, 2)
                for u1, u2, data in graph.edges(data=True)
            },
        )
        nx.draw_networkx_labels(
            graph, positioning, labels={user: user_to_str(user) for user in graph.nodes}
        )

        nx.draw_networkx_edges(graph, positioning, arrows=True, arrowsize=19)
        if output_filename is None:
            plt.show()
        else:
            assert file_extension is not None
            plt.savefig(output_filename, format=file_extension, bbox_inches="tight")
            plt.close()
            if return_file_content:
                s = io.BytesIO()
                with open(output_filename, "rb") as fp:
                    s.write(fp.read())

                os.unlink(output_filename)
                return s.getvalue().decode()


def update_user_debts():
    """Overwrites all specified users' debts using the data from payments."""
    new_payments: QuerySet = Payment.objects.to_be_added_to_graph()
    if not list(new_payments):
        logger.debug("no new payments")
        return

    users = (
        User.objects.prefetch_related("performed_payments", "as_debtor")
        .filter(
            Q(performed_payments__in=new_payments)
            | Q(as_debtor__payment__in=new_payments)
        )
        .distinct()
    )

    old_debts: QuerySet = Debt.objects.all()
    if not list(old_debts):
        logger.info("generating graph from new payments")
        try:
            graph_service_whole = GraphService.from_payments_list(list(new_payments), users)
        except EmptyGraphError:
            logger.warning("tried to create an empty graph, quitting")
            return
    else:
        logger.info("generating graph from old debts")
        graph_service_old = GraphService.from_debts_list(list(old_debts))
        try:
            graph_service_new = GraphService.from_payments_list(list(new_payments), users)
        except EmptyGraphError:
            logger.warning("new payments amounted to an empty graph 🤷")
            graph_service_whole = graph_service_old
        else:
            graph_service_whole = graph_service_old + graph_service_new

    graph_service_whole.minimize_debt_graph()
    new_debts: List[Debt] = graph_service_whole.to_debts_list(added_automatically=True)

    with transaction.atomic():
        old_debts.delete()
        Debt.objects.bulk_create(new_debts)
        new_payments.update(added_to_graph=True)

    return new_debts
