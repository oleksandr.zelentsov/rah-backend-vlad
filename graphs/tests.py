from copy import deepcopy

import pytest

from accounts.recipes import user_recipe
from debts.models import Debt
from debts.testing_utils import generate_debts, debt_info_to_model_data
from graphs.errors import EmptyGraphError
from graphs.services import GraphService, update_user_debts
from graphs.testing_utils import integrity_info_to_graph_integrity
from payments.models import Payment
from payments.recipes import payment_recipe
from payments.testing_utils import generate_payments_and_debtors
from utils.helpers import money_is_close

GRAPH_INTEGRITY_DATA = [
    (
        (  # Debts: who_owes_idx, who_is_owed_idx, debt_value
            (1, 0, 10),
            (2, 0, 20),
            (3, 0, 20),
        ),
        {0: +50, 1: -10, 2: -20, 3: -20},
    ),
    (
        (  # Debts: who_owes_idx, who_is_owed_idx, debt_value
            (0, 1, 50),
            (0, 2, 20),
            (3, 0, 60),
            (1, 3, 40),
        ),
        {0: -10, 1: +10, 2: +20, 3: -20},
    ),
    # (
    #     (  # Debts: who_owes_idx, who_is_owed_idx, debt_value
    #         (0, 1, 50),
    #         (0, 2, 20),
    #         (3, 0, 60),
    #         (1, 3, 40),
    #         (1, 3, 20),
    #     ),
    #     {
    #         0: -10,
    #         1: +10,
    #         2: +20,
    #         3: -20,
    #     }
    # ),
]


@pytest.mark.django_db
def test_build_debt_graph_basic():
    users = user_recipe.make(_quantity=120)
    debts = generate_debts(users)
    graph_service = GraphService.from_debts_list(debts)
    assert len(list(graph_service.graph.edges)) == len(
        debts
    ), "every individual debt should be reflected as an edge"
    for debt in debts:
        assert money_is_close(
            graph_service.get_debt_value(debt.user_1, debt.user_2), debt.debt_value
        )

    print(graph_service.graph)


@pytest.mark.django_db
def test_minimize_debt_graph_basic():
    users = user_recipe.make(_quantity=60)
    debts = generate_debts(users)
    graph_service = GraphService.from_debts_list(debts)

    complex_graph = deepcopy(graph_service.graph)
    graph_service.minimize_debt_graph()
    simple_graph = deepcopy(graph_service.graph)

    assert GraphService.are_graphs_equivalent(complex_graph, simple_graph)


@pytest.mark.django_db
def test_update_user_debts():
    users = user_recipe.make(_quantity=15)
    generate_payments_and_debtors(users)
    update_user_debts()
    gs = GraphService.from_debts_list(Debt.objects.involved_debts(users))
    assert gs.is_minimized


@pytest.mark.django_db
def test_update_user_debts_no_payments():
    users = user_recipe.make(_quantity=15)
    # do not use generate_payments_and_debtors(users)
    update_user_debts()
    with pytest.raises(EmptyGraphError):
        gs = GraphService.from_debts_list(Debt.objects.involved_debts(users))
        assert gs.is_minimized


@pytest.mark.django_db
def test_merge_graphs():
    users = user_recipe.make(_quantity=20)
    users_part_1 = users[:10]
    users_part_2 = users[5:]

    payments_list_1, _ = generate_payments_and_debtors(users_part_1)
    payments_list_2, _ = generate_payments_and_debtors(users_part_2)
    payments_list = payments_list_1 + payments_list_2

    debts = GraphService.payments_to_debts(payments_list)
    Debt.objects.bulk_create(debts)

    gs1: GraphService = GraphService.from_debts_list(
        Debt.objects.involved_debts(users_part_1)
    )
    g1_integrity = gs1.graph_integrity
    gs2: GraphService = GraphService.from_debts_list(
        Debt.objects.involved_debts(users_part_2)
    )
    g2_integrity = gs2.graph_integrity

    gs_whole: GraphService = gs1.merge_graphs(gs2)

    whole_integrity = gs_whole.graph_integrity
    for user, value in whole_integrity.items():
        g1_user_integrity = g1_integrity.get(user, gs1.zero.amount)
        g2_user_integrity = g2_integrity.get(user, gs2.zero.amount)
        integrity_sum = g1_user_integrity + g2_user_integrity

        assert money_is_close(
            value, integrity_sum
        ), f"{value} should be close to {integrity_sum} = {g1_user_integrity} + {g2_user_integrity}"


@pytest.mark.django_db
@pytest.mark.parametrize("debts_info,integrity_info", GRAPH_INTEGRITY_DATA)
def test_graph_integrity(debts_info, integrity_info):
    max_user_index = max([max(x1, x2) for x1, x2, _ in debts_info])
    users = user_recipe.make(_quantity=max_user_index + 1)
    debts = debt_info_to_model_data(debts_info, users)

    gs = GraphService.from_debts_list(debts)
    graph_integrity = integrity_info_to_graph_integrity(
        integrity_info, users, gs.currency
    )

    assert gs.graph_integrity == graph_integrity


@pytest.mark.django_db
def test_update_user_debts_has_old_debts():
    users = user_recipe.make(_quantity=15)
    generate_payments_and_debtors(users)
    update_user_debts()

    generate_payments_and_debtors(users)
    update_user_debts()

    gs = GraphService.from_debts_list(Debt.objects.involved_debts(users))
    assert gs.is_minimized


@pytest.mark.django_db
def test_update_user_debts_has_new_user():
    users = user_recipe.make(_quantity=15)
    generate_payments_and_debtors(users)
    update_user_debts()

    users += user_recipe.make(_quantity=5)

    generate_payments_and_debtors(users)
    update_user_debts()

    gs = GraphService.from_debts_list(Debt.objects.involved_debts(users))
    assert gs.is_minimized


@pytest.mark.django_db
def test_resolve_parallel_debts():
    u1, u2 = user_recipe.make(_quantity=2)
    payment12: Payment = payment_recipe.make(payer_id=u1.id, payment_value=10)

    d11, d12 = (
        payment12.add_debtor(u1, 1),
        payment12.add_debtor(u2, 1),
    )

    d11.confirm()
    d12.confirm()
    update_user_debts()

    debt_21 = Debt.objects.get(user_1=u2, user_2=u1)
    assert money_is_close(debt_21.debt_value, 5)

    payment21: Payment = payment_recipe.make(payer_id=u2.id, payment_value=10)
    d21, d22 = (
        payment21.add_debtor(u2, 1),
        payment21.add_debtor(u1, 1),
    )
    d21.confirm()
    d22.confirm()
    update_user_debts()

    assert not Debt.objects.filter(user_1__in=[u1, u2], user_2__in=[u1, u2]).exists()


@pytest.mark.django_db
def test_resolve_parallel_debts_first():
    u1, u2 = user_recipe.make(_quantity=2)
    payment12: Payment = payment_recipe.make(payer_id=u1.id, payment_value=10)
    payment21: Payment = payment_recipe.make(payer_id=u2.id, payment_value=10)

    d11, d12, d21, d22 = (
        payment12.add_debtor(u1, 1),
        payment12.add_debtor(u2, 1),
        payment21.add_debtor(u2, 1),
        payment21.add_debtor(u1, 1),
    )

    d11.confirm()
    d12.confirm()
    update_user_debts()

    debt_21 = Debt.objects.get(user_1=u2, user_2=u1)
    assert money_is_close(debt_21.debt_value, 5)

    d21.confirm()
    d22.confirm()
    update_user_debts()

    assert not Debt.objects.filter(user_1__in=[u1, u2], user_2__in=[u1, u2]).exists()
