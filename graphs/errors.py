from django.utils.translation import ugettext


class GraphError(Exception):
    message = None

    def __str__(self):
        return self.message


class EmptyGraphError(GraphError):
    message = ugettext("cannot use empty graph")


class NoCurrencyGraphError(GraphError):
    message = ugettext("cannot use graph with no currency")


class TooManyCurrenciesGraphError(GraphError):
    message = ugettext("cannot use graph with no currency")
