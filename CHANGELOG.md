# Changelog

## Untracked changes

## [1.1.2] - 2020-02-08
- [imp] parametrized markdown page urls for navigation
- [imp] parametrized server details and created a data structure for it

## [1.1.1] - 2020-02-08
- [imp] literally added this page as a page at `/`;
- [imp] added page with arbitrary server info at `/server-info`;
- both pages have urls and are constructed from Markdown

## [1.1.0] - 2020-02-07
- [bug] possibility of payments with no "own part" debtors, no exception on their confirmation
- [imp] some Polish translations
- [imp] automatic closing of popup payment create form

## [1.0.3] - 2020-01-26
- [bug] django_extensions in requirements.txt

## [1.0.2] - 2020-01-26
- [bug] display of debts to have the real names instead of emails;
- [imp] an alert with "Błąd pobierania płatności.";
- [imp] added parsing of description;

## [1.0.0] - 2019-12-30
The initial release with all the basic features.